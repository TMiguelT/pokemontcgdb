module.exports = {
    cardStore: require("./stores/cardStore"),
    filterStore: require("./stores/filterStore"),
    optionStore: require("./stores/optionStore")
};