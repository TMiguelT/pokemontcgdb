var _ = require("lodash");

var types = [
    {
        groupName: "Miscellaneous",
        items: [
            {
                name: "Remove Duplicates",
                value: "removeDuplicates"
            },
            {
                name: "Has Ability",
                value: "hasAbility"
            },
            {
                name: "No EXs",
                value: "noEx"
            }
        ]
    },
    {
        groupName: "Card Text",
        items: [
            {
                name: "Card Name",
                value: "cardName",
                hint: "(contains the following text)"
            },
            {
                name: "Card Text",
                value: "cardTextContains",
                hint: "(contains the following text)"
            },
            {
                name: "Attack Text",
                value: "attackTextContains",
                hint: "(contains the following text)"
            },
            {
                name: "Ability Text",
                value: "abilityContains",
                hint: "(contains the following text)"
            }
        ]
    },
    {
        groupName: "Weakness and Resistance",
        items: [
            {
                name: "Resistant To",
                value: "resistance"
            },
            {
                name: "Weak To",
                value: "weakness"
            }
        ]
    },
    {
        groupName: "Type",
        items: [
            {
                name: "Colour",
                value: "colour",
                hint: "(is one of)"
            },
            {
                name: "Basic Type",
                value: "basicType",
                hint: "(is one of)"
            },
            {
                name: "Specific Type",
                value: "specificType",
                hint: "(is one of)"
            },
            {
                name: "Evolves From/To",
                value: "evolves",
                hint: "(in the same evolutionary tree as)"
            }
        ]
    },
    {
        groupName: "Stats",
        items: [
            {
                name: "HP Greater Than",
                value: "hpGt"
            },
            {
                name: "HP Less Than",
                value: "hpLt"
            },
            {
                name: "Retreat Cost Less Than",
                value: "rcLt"
            },
            {
                name: "Retreat Cost More Than",
                value: "rcGt"
            }
        ]
    }

];

var map = _.chain(types).map("items").flatten().indexBy("value").value();

module.exports = {
    array: types,
    map: map
};