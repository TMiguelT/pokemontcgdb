var React = require('react');
var Image = require('react-image-preloader');
var InfiniteScroll = require('react-infinite-scroll')(React);

var actions = require("../actions");
//http://assets7.pokemon.com/assets/cms2/img/cards/web/cardback.png
var Cards = React.createClass({
    loadMore: function () {
        actions.pageScroll();
    },

    render: function () {
        return React.cloneElement(this.doRender(), {
            style: {display: "inline"},
            className: "well"
        });
    },

    doRender: function () {
        return (
            <InfiniteScroll
                pageStart={0}
                loadMore={this.loadMore}
                hasMore={this.props.hasMore}
                loader={<div className="loader">Loading ...</div>}>
                {this.props.cards.map(function (card) {
                    return (
                        <a href={card.url}>
                            <Image fallback="http://assets7.pokemon.com/assets/cms2/img/cards/web/cardback.png"
                                   key={card.image} src={card.image}
                                   style={{display: "inline"}}/>
                        </a>
                    );
                })}
            </InfiniteScroll>
        );
    }
});

module.exports = Cards;