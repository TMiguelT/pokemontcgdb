var React = require('react');
var Bootstrap = require('react-bootstrap');
var filterMap = require("../presetData/filterTypes").map;
var actions = require("../actions");
var energies = require("../presetData/pokemonColours");

var Filter = React.createClass({

    onChange: function () {
        this.props.onChange(this.refs.input.getValue())
    },

    render: function () {

        //Add various attributes to the input based on the type of filter
        var props = {
            type: 'text'
        };

        var childEls = null;

        switch (this.props.type) {

            case "weakness":
            case "resistance":
            case "colour":
                childEls = this.props.options.energies.map(function (energy) {
                    return (<option value={energy}>{energy}</option>)
                });
                props = {
                    multiple: true,
                    type: "select"
                };
                break;

            case "basicType":
                childEls = this.props.options.basicTypes.map(function (type) {
                    return (<option value={type}>{type}</option>)
                });
                props = {
                    size: 3,
                    multiple: true,
                    type: "select"
                };
                break;

            case "specificType" :
                childEls = this.props.options.specificTypes.map(function (type) {
                    return (<optgroup label={type.group}>
                        {type.options.map(function (opt) {
                            return <option value={opt}>{opt}</option>
                        })}
                    </optgroup>)
                });
                props = {
                    size: 15,
                    multiple: true,
                    type: "select"
                };
                break;

            case "rcGt":
            case "rcLt":
            case "hpGt":
            case "hpLt":
                props.type = "number";
                props.step = 10;
                break;

            case "noEx":
                props.value = "Do not include pokemon-EX";
                props.disabled = true;
                break;

            case "hasAbility":
                props.value = "Has a passive ability";
                props.disabled = true;
                break;

            case "removeDuplicates":
                props.value = "Only include unique results";
                props.disabled = true;
                break;
        }

        //Calculate the label
        var filterData = filterMap[this.props.type];
        var label = filterData.name + " " + (filterData.hint || "");

        //Render the input
        return (

            <Bootstrap.Input
                {...props}
                ref="input"
                defaultValue={this.props.value}
                label={label}
                onChange={this.onChange}
                buttonAfter={
                    <Bootstrap.Button onClick={this.props.onDelete}>
                        <Bootstrap.Glyphicon
                            glyph='remove'
                            style={{cursor: "pointer"}}
                        />
                        </Bootstrap.Button>
                    }>
                {childEls}
            </Bootstrap.Input>
        );
    }
});

module.exports = Filter;