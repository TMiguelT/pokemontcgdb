var React = require('react');
var Bootstrap = require('react-bootstrap');
var Filter = require("./filter.jsx");
var AddFilter = require("./addFilterModal");
var actions = require("../actions");

//Filters, a constant set of options for the query
var filterTypes = require("../presetData/filterTypes").array;

var Filters = React.createClass({
    removeAt: function (id) {
        actions.removeFilter(id);
    },

    editAt: function (id, value) {
        actions.editFilter(id, value);
    },

    handleKey: function (e) {
        if (e.keyCode == 13 ) {
            actions.query(1);
        }
    },

    render: function () {
        var self = this;

        var panelHeader = (
            <div>
                <h4>Filters</h4>
            </div>
        );
        return (
            <div className="responsive-form" onKeyDown={this.handleKey}>
                <Bootstrap.Panel
                    collapsable
                    defaultExpanded
                    header={panelHeader}
                    footer={
                        <Bootstrap.ModalTrigger modal={<AddFilter filterTypes={filterTypes}/>}>
                           <Bootstrap.Button>
                           Add Filter
                                {/*<Bootstrap.Glyphicon glyph="plus"/>*/}
                            </Bootstrap.Button>
                        </Bootstrap.ModalTrigger>
                    }>
                    {this.props.filters.map(function (filter) {
                        return (
                            <Filter
                                options={self.props.options}
                                key={filter.id}
                                type={filter.type}
                                value={filter.value}
                                onDelete={self.removeAt.bind(self, filter.id)}
                                onChange={self.editAt.bind(self, filter.id)}
                                />
                        )
                    })}

                </Bootstrap.Panel>
            </div>
        );
    }
});

module.exports = Filters;