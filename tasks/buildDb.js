var knex = require("./db");
var createQuery =
    `CREATE TABLE IF NOT EXISTS cards(
        data jsonb
    );`;
module.exports = function* () {
    yield knex.raw(createQuery);
    knex.destroy();
};