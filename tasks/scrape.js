var knex = require("./db");
var scraper = require("pokemon-tcg-scraper");

module.exports = function* () {
    var cards = yield scraper.scrapeAll({format: "expanded"});
    var hashes = cards.map(function (card) {
        return {data: card};
    });
    yield knex("cards").insert(hashes);
    knex.destroy();
};