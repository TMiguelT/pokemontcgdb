var knex = require('knex');
//Either use the heroku DB, or use the local one
var dbUrl = process.env.DATABASE_URL;
module.exports = require('knex')({
    client: 'pg',
    connection: dbUrl
});