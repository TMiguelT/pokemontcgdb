module.exports = function *() {
    var ret = {
        energies: (yield this.knex.raw(
            `SELECT DISTINCT data->>'color' AS color
            FROM cards
            WHERE (data->'color') IS NOT NULL`
        )).rows.map(function (el) {
            return el.color;
        }),

        basicTypes: (yield this.knex.raw(
            `SELECT DISTINCT data->>'superType' AS type
            FROM cards`
        )).rows.map(function (el) {
            return el.type;
        }),

        specificTypes: (yield this.knex.raw(
            `SELECT data->>'superType' as group, json_agg(DISTINCT data->>'type') as options
             FROM cards
             GROUP BY data->>'superType'`
        )).rows
    };

    console.log(ret);

    this.body = ret;
};