const PAGE_SIZE = 20;

module.exports = function *() {
    var params = this.request.query;
    var page = "page" in params ? params.page : 1;
    console.log(params.filters);
    var filters = "filters" in params ? JSON.parse(params.filters) : [];
    console.log(filters);

    var query = this.knex("cards")
        .offset((page - 1) * PAGE_SIZE)
        .limit(PAGE_SIZE);

    var self = this;

    //If they have no query, randomise the resutls
    if (filters.length == 0)
        query.orderByRaw('random()');

    //If they have a query, filter the results
    filters.forEach(function (filter) {
        switch (filter.type) {

            case "noEx":
                query = query.whereRaw(`data->>'type' NOT LIKE '%EX%'`, filter.value);
                break;

            case "cardName":
                query = query.whereRaw(`data->>'name' ILIKE '%' || ? || '%'`, [filter.value]);
                break;

            case "removeDuplicates":
                query = query.select(self.knex.raw(
                    `DISTINCT ON (data->'name', data->'abilities', data->'passive')
                    "data"`));
                break;

            case "basicType":
                query = query.whereRaw(`data->>'superType' = ANY(?)`, [filter.value]);
                break;

            case "specificType":
                query = query.whereRaw(`data->>'type' = ANY(?)`, [filter.value]);
                break;

            case "cardTextContains":
                query = query.whereRaw(
                    `concat(data->>'text', data->>'passive', data->>'abilities') ILIKE concat('%', ?::text, '%') `, [filter.value]);
                break;

            case "attackTextContains":
                query = query.whereRaw(
                    `data->>'abilities' ILIKE '%' || ? || '%' `, [filter.value]);
                break;

            case "hasAbility":
                query = query.whereRaw(`data->>'passive' != ''`);
                break;

            case "evolves":
                query = query.whereRaw(
                    `? = ANY (
                    SELECT pre_cards.data->>'name'
                    FROM
                        pre_evolutions pre
                        JOIN cards pre_cards ON pre.pre_evo_id = pre_cards.id
                    WHERE pre.id = cards.id

                    UNION

                    SELECT post_cards.data->>'name'
                    FROM
                        pre_evolutions post
                        JOIN cards post_cards ON post.id = post_cards.id
                    WHERE post.pre_evo_id = cards.id
                )`, filter.value);

                break;

            case "resistance":
                query = query.whereRaw(`? = ANY (SELECT res->>'type' FROM jsonb_array_elements(data->'resistances') res)`, filter.value);
                break;

            case "weakness":
                query = query.whereRaw(`? = ANY (SELECT weak->>'type' FROM jsonb_array_elements(data->'weaknesses') weak)`, filter.value);
                break;

            case "abilityContains":
                query = query.whereRaw(`data->>'passive' ILIKE '%' || ? || '%'`, [filter.value]);
                break;

            case "colour":
                query = query.whereRaw(`data->>'color' = ANY(?)`, [filter.value]);
                break;

            case "hpGt":
                query = query.whereRaw(`(data->>'hp')::integer > ?`, [filter.value]);
                break;

            case "hpLt":
                query = query.whereRaw(`(data->>'hp')::integer < ?`, [filter.value]);
                break;

            case "rcGt":
                query = query.whereRaw(`(data->>'retreatCost')::integer > ?`, [filter.value]);
                break;

            case "rcLt":
                query = query.whereRaw(`(data->>'retreatCost')::integer < ?`, [filter.value]);
                break;
        }
    });

    var cards = yield query.map(function (card) {
        return card.data;
    });

    console.log(query.toSQL());

    if (cards.length <= 0)
        this.body = false;
    else
        this.body = cards;
};