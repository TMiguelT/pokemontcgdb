DROP MATERIALIZED VIEW card_evolutions;
CREATE MATERIALIZED VIEW card_evolutions AS
WITH RECURSIVE cards_evolution
AS (
  -- get parent
  SELECT id, data, NULL AS parent
  FROM cards
  WHERE data->>'evolvesFrom' != ''

  UNION

  -- get all parents
  SELECT c.id, c.data, ce.id AS parent
  FROM cards_evolution ce
  JOIN cards c
  ON 
	ce.data->'evolvesFrom' = c.data->'name' 
	OR c.data->'evolvesFrom' = ce.data->'name'
	OR ce.parent = c.id
)
SELECT id, parent AS evolution from cards_evolution