var gulp = require('gulp');
var Promise = require('bluebird');
var watchify = require('watchify');
var reactify = require('reactify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');
var uglify = require('gulp-uglify');

var opts = assign({}, watchify.args, {
    entries: ['./src/app.jsx'],
    debug: true,
    transform: [reactify]
});
var b = watchify(browserify(opts));
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', console.log); // output build logs to terminal

function bundle(){
    return b
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist'));
}

gulp.task("scrape", function () {
    var scrape = require("./tasks/scrape");
    Promise.coroutine(scrape)();
});

gulp.task("buildDb", function () {
    var build = require("./tasks/buildDb");
    Promise.coroutine(build)();
});

gulp.task("build", ["css", "js"]);

gulp.task("css", function () {
    gulp.src("css/*")
        .pipe(gulp.dest("./dist"));
});

gulp.task("js", bundle);