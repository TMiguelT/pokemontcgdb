var parse = require('pg-connection-string').parse;
var connection;

if ("DATABASE_URL" in process.env)
    connection = parse(process.env.DATABASE_URL);
else
    connection = {
        port: 5433,
        host: 'localhost',
        user: 'postgres',
        password: 'Proline185',
        database: 'pokemon'
    };

module.exports = connection;